# Databricks notebook source
# MAGIC %md 
# MAGIC #Comparing strengths and weaknesses of NLP techniques
# MAGIC 
# MAGIC ### State of the Art in NLP: Transformers & BERT (BERTje/RobBERT/DistilBERT)
# MAGIC 
# MAGIC In a sequence of articles we compare different NLP techniques to show you how we get valuable information from unstructured text. About a year ago we gathered reviews on Dutch restaurants. We were wondering whether 'the wisdom of the croud' - reviews from restaurant visitors - could be used to predict which restaurants are most likely to receive a new Michelin-star. Read [this post](https://cmotions.nl/en/who-will-receive-michelin-star-2020/ "Who will receive a Michelin star…? We already know!") to see how that worked out. We used [topic modeling](http://www.cs.columbia.edu/~blei/papers/Blei2012.pdf) as our primary tool to extract information from the review texts and combined that with predictive modeling techniques to end up with our predictions. 
# MAGIC 
# MAGIC We got a lot of attention with our predictions and also questions about how we did the text analysis part. To answer these questions, we explain our approach in more detail in a series of articles on NLP. We didn't stop exploring NLP techniques after our publication, and we also like to share insights from adding more novel NLP techniques. More specifically we will use two types of word embeddings -  a classic [Word2Vec model](https://arxiv.org/abs/1301.3781) and a [GLoVe embedding model](https://nlp.stanford.edu/projects/glove/) - we'll use transfer learning with pretrained word embeddings and we use transformers like [BERT](https://arxiv.org/abs/1810.04805v2). We compare the added value of these advanced NLP techniques to our baseline topic model on the same dataset. By showing what we did and how we did it, we hope to guide others that are keen to use textual data for their own data science endeavours. 
# MAGIC 
# MAGIC In our earlier articles we [extracted topic models](https://www.theanalyticslab.nl/nlpblogs_1_identifying_topics_using_topic_-modeling_lda/) and [word embeddings](https://www.theanalyticslab.nl/nlpblogs_2_training_word_embedding_models_and_visualize_results/) from our review texts. We showed how well these [topic models were in predicting michelin reviews](https://www.theanalyticslab.nl/nlpblogs_3_use_topic_modeling_results_in_predictive_modeling/). And finally, we showed how these predictions could be improved substantially using the [word embeddings to predict michelin stars](https://www.theanalyticslab.nl/nlpblogs_4_using_word_embedding_models_for_prediction_purposes/).   
# MAGIC 
# MAGIC > In this article we introduce **Transformers** and show you how state of the art NLP techniques like *BERT* (dutch versions BERTje and RobBERT and multilingual distilBERT) can be used to transform our review text in context dependent representations and how to use those in a downstream prediction task.   

# COMMAND ----------

# MAGIC %md
# MAGIC #### Transformers - what's so special?
# MAGIC 
# MAGIC In our previous blogs, we introduced a number of word embedding techniques. We showed how word embeddings translate words into numeric vectors that represent the meaning of words. Some time ago, word embeddings were the state of the art in NLP, but then came Transformers and some say this has revolutionalized how data scientists work with textual data. What are transformers and why are they so special?
# MAGIC 
# MAGIC ##### The importance of the sequence & context in texts
# MAGIC 
# MAGIC To really understand a sentence or piece of text, it's essential to look at the words in the text in relation to the surrounding words. Text is sequential data and for many use cases this ordering is important. It's not always crucial though! Remember our [Topic Model](https://www.theanalyticslab.nl/nlpblogs_1_identifying_topics_using_topic_-modeling_lda/) to derive topics in the restaurant reviews? Here we fully neglected the order and treated all reviews as 'bags of words'. This resulted in topics that are easily understood and the distribution over topics proved to be [quite powerful in predicting Michelin vs non-Michelin reviews](https://www.theanalyticslab.nl/nlp-use-topic-modeling-results-in-predictive-modeling). Also, our [word embeddings](https://www.theanalyticslab.nl/nlp-use-word-embeddings-in-predictive-modeling) didn't take the exact order of words into account when predicting michelin reviews even better than we could with topic scores. However, in more complex tasks - for instance when you want to translate a review, reply to the review or generate other meaningful, human-like texts - the sequence does matter a great deal. And possibly, our 'simple' prediction task also improves when we use a model with deeper 'understanding' of the review than we did so far with our topic model and word embeddings! We'll see...
# MAGIC 
# MAGIC ##### Before BERT: Transformers
# MAGIC 
# MAGIC In the field of Neural Networks, dramatic improvements are made in the past decade how to deal with sequential data - time series, audio & video and textual data to name a few. Within textual data, most progress is made in developing highly accurate sequence-to-sequence models, used in domains such as speech recognition, machine translation, text summarization and question answering. Recurrent and Convolutional Neural Network architectures and later variants (LSTM, GNU) pushed performance in several NLP tasks, but at growing computational costs and training time. Sequential models are not very suitable to parallelize and distribute over multiple cores...  **Transformers** have set a new standard by both improving performance of these recurrent neural network configurations by better dealing with long range (more distant) dependencies between words and also improving performance dramatically by enabling more paralellization in training. For more details on Transformers: the seminal paper introducing Transformers is the ['Attention is all you Need' article](https://arxiv.org/abs/1706.03762) mainly developed at Google Research. Although these Transformer models already showed groundbreaking improvements in results in many NLP tasks compared to its predecessors, its variant BERT lifted the bar even further. For all the details on BERT, read the [original paper](https://arxiv.org/abs/1810.04805) or [this nice blog about BERT](http://jalammar.github.io/illustrated-bert/). 
# MAGIC 
# MAGIC ##### After BERT: GPT-2, XLNet, GPT-3 ...
# MAGIC 
# MAGIC Developments in NLP haven't stopped after BERT: GPT-2, XLNet and more recently [GPT-3](https://www.technologyreview.com/2020/07/20/1005454/openai-machine-learning-language-generator-gpt-3-nlp/) were introduced with stunning performance on NLP tasks. In this blog we focus on BERT for a number of reasons. First of all, much of the increased performance reported are in the domain of text generation (completing texts, question answering, conversational contexts) where the focus is on how 'human' the generated texts are. In our context, we want to emphasize on a context where you can utilize NLP techniques in other downstream tasks like predictive modeling. Secondly, these improvements also come at a price: parameters to be trained! GPT-2 has *1,5 billion* parameters and GPT-3 tops this with *175 billion parameters* to train! Our BERT model *only* has *109 million* parameters, already quite a [carbon footprint](https://arxiv.org/pdf/1906.02243.pdf). Finally, if you do want to use these later models, this is only a small change to approach we take in our blog. You just take another model from the Hugging Face model repo we will introduce later on and you're ready to go. You might need some extra GPU/TPU to run these, though... 
# MAGIC 
# MAGIC ##### Who's BERT?
# MAGIC 
# MAGIC So who's BERT? The name BERT is more than a fun reference to [Elmo](https://arxiv.org/pdf/1802.05365.pdf), the *deep contextualized word representations* and one of the main predecessors of BERT. In fact it's an acronym that stands for **B**idirectional **E**ncoder **R**epresentations from **T**ransformers. As its name suggest, it's still a Transformer model, but one that is trained looking in both directions (left-to-right *and* right-to-left) when processing a text sequence, whereas prior techniques had a unidirectional approach (left-to-right *or* right-to-left). And BERT results in high value pretrained text representations, that can be finetuned for the NLP task at hand. Whereas by reading the papers on BERT it becomes clear *how* BERT works, it's not that obvious *why* it performs so well. This [paper](https://arxiv.org/abs/2002.12327) does a nice job in summarizing what is known on BERT so far and provides a better, deeper understanding of BERT without drowning the reader in too many technical details (still quite some, though).  
# MAGIC 
# MAGIC ##### How BERT is trained and used
# MAGIC 
# MAGIC The training of BERT is done in two ways: First, random words in the sentences from the training data are masked and the model needs to predict these words from its context. Secondly, half of all subsequent sentences in the training data are swapped and the model has to figure out which are in the right order and which are swapped. In later variants of BERT like Roberta and Distilbert these two pretraining tasks are somewhat altered and performance is futher improved. For all its variants, training results in the pretrained model that can be downloaded and used to finetune for another NLP task. And that's what we'll do in this article.
# MAGIC 
# MAGIC Until Transformer models appeared, training and using NLP models was available to the happy few that had access to huge computational resources, relevant data and budgets. Transformer models are pretrained on massive datasets and are made available for downloading and finetuning, requiring only a fraction of resources needed for finetuning compared to pretraining. Therefore, many are able to (re)use the greatness of models trained on huge datasets at enourmous costs in their NLP tasks with minimal investments needed in resources to finetune these models. 
# MAGIC 
# MAGIC Main  advantages of BERT are that it is a general purpose model that can handle an arbitrary length as input, is already pretrained and is available to everyone. Previous NLP models usually were trained for a specific task at hand, BERT however can be used for various NLP tasks, it only needs to be finetuned for the specific task. This saves the NLP practitioner loads of time and money on GPUs and data collection to train the full Transformer model. 
# MAGIC 
# MAGIC ##### BERT for other languages
# MAGIC 
# MAGIC Although machine translation is an important use case for many NLP models, most emphasis in developing state of the art NLP techniques is put on English texts. For our analysis, we are interested in Dutch Restaurant Reviews and need a language model that performs best on Dutch words. Fortunately, BERT comes with a multilanguage variant, optimized for numerous languages. And there are some custom BERT variants, such as BERTje and RobBERT, further optimized for Dutch. We'll have a look at these alternatives when using BERT to predict Michelin stars with our restaurant reviews. <br>
# MAGIC 
# MAGIC 
# MAGIC !['BERT models used in this article'](https://bhciaaablob.blob.core.windows.net/cmotionsnlpblogs/Header blog 5 small.png)<br>

# COMMAND ----------

# MAGIC %md
# MAGIC #### Before we start: Let's have a bit of python with our R!
# MAGIC 
# MAGIC So far, we've used R and R only in our NLP endeavours to translate review texts into value. We did so for a reason. Despite python's popularity and despite we also use python a lot in many of our day to day Data Science tasks, we still believe R has much to offer for data scientists - also in the field of text analytics and deep learning - and we know there's a great community of R users out there, looking for interesting use cases in R. We also see that more options become available to *combine* the greatness of both R and python; [**reticulate**](https://rstudio.github.io/reticulate/) is one of them. This package enables you to use python code in R almost seamlessly. 
# MAGIC 
# MAGIC We use the reticulate package here, since currently one of the easiest ways to use Transformer models including BERT is to use the python library [transformers](https://github.com/huggingface/transformers). This great piece of work developed by [Hugging Face 🤗](https://huggingface.co/) provides many pretrained models, datasets, APIs, tutorials and much more. We will use it here to get access to pretrained `BERT` models for the Dutch language and to finetune those to build our prediction models.

# COMMAND ----------

# Load libraries needed in this blog
library(tidyverse)
library(reticulate)
library(modelplotr)
library(keras)

# COMMAND ----------

# bits of python needed in this blog (python in r thanks to reticulate)  
reticulate::py_install('transformers', pip = TRUE)
transformer = reticulate::import('transformers')
tf = reticulate::import('tensorflow')
builtins <- import_builtins() #built in python methods

# COMMAND ----------

# MAGIC %md We are going to be using a large pretrained model and need all the GPU power we can get in this notebook. In Azure Databricks we've set up two 'NVIDEA Tesla K80' [GPUs](https://www.nvidia.com/en-gb/data-center/tesla-k80/) that have a memory size of 11GB and a bandwith of 223GB/s. Below we make sure that GPU input is managed well and we utilise both GPUs. 

# COMMAND ----------

# Allow more memory efficient handling of GPUs
GPUs <- tf$config$list_physical_devices(device_type='GPU')
tf$config$experimental$VirtualDeviceConfiguration(memory_limit=4000)#  
tf$config$experimental$set_memory_growth(GPUs[[1]], enable=T)
tf$config$experimental$set_memory_growth(GPUs[[2]], enable=T)

# COMMAND ----------

# MAGIC %md
# MAGIC We're ready to start using BERT and prepare it for finetuning on our prediction task to predict Michelin reviews like we did with our topic modeling results and our word embedding results. As you see below, when you load a python module in R with reticulate, it works like any other object in R, you can call the contents of the module, mainly its methods and functions, using the `$` sign, as you would do if you want to get something from any other R object. Here, we start by loading the module using the reticulate `import`-function and we use its method to download the model from the transformers package. We start by using BERTje, a model built for the Dutch language, developed by [Wietse de Vries](https://arxiv.org/abs/1912.09582). BERTje was trained on high quality Dutch text that include books, news corpora, news web pages, Wikipedia and [SoNaR-500](http://portal.clarin.nl/node/14365), a 500-million-word reference corpus of contemporary written Dutch.
# MAGIC 
# MAGIC After loading BERT - or BERTje which has the same configuration - we can explore how BERT works. The key elements we need are the tokenizer and the pretrained model. The tokenizer that comes with the model will be used to look up the dutch words in our restaurant reviews and map them to the token ids needed by BERT. Aside from these token ids, the tokenizer also adds some generic tokens to the text sequences: 
# MAGIC - [UNK] for tokens in the text sequence that are not in the vocabulary of 30.000 tokens used for training, 
# MAGIC - [CLS] is a special token used to determine the start of each sequence
# MAGIC - [SEP] is a separator token used to separate parts within a sequence (sentences, question/answers)
# MAGIC - [PAD] is the token used to fill sequences that are shorter than the specified sequence length used in the model
# MAGIC - [MASK] is the token used in pretraining, when a sample of tokens is masked to train the Masked LM model
# MAGIC 
# MAGIC This visualization from the [original paper](https://arxiv.org/pdf/1810.04805.pdf) shows how BERT tokenizes the textual data for pretraining: 
# MAGIC <img src ='https://blogs.rstudio.com/ai/posts/2019-09-30-bert-r/images/emb.png'>

# COMMAND ----------

# MAGIC %md
# MAGIC The [MASK] and [SEP] tokens are essential for pretraining BERT. [SEP] tells BERT what the first sentence and what the second sentence is in the provided text. Remember that training of BERT is done in two ways: by learning what are words that are randomly masked before training (the [MASK] tokens that are added before pretraining) and by figuring out what is the right order of the sentences within the text. Since we will not redo the pretraining of the model, we will not need to split our texts into sentences divided by the [SEP] token and we don't need to add any [MASK] tokens as well. We can use the tokenizer we need (each model has its own way to tokenize the text to fit the model structure) from the Transformers package. Thereafter, we can load our own textual data and apply the tokenizer to it.

# COMMAND ----------

# get BERTje tokenizer
tokenizer <- transformer$AutoTokenizer$from_pretrained('wietsedv/bert-base-dutch-cased')

# Print some examples from the vocabulary
print(tokenizer$vocab[1:5])
print(tokenizer$vocab[9001:9006])


# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Load preprocessed data 
# MAGIC 
# MAGIC Let's load the restaurant review data we've [prepared in an earlier blog](https://www.theanalyticslab.nl/nlpblogs_0_preparing_restaurant_review_data_for_nlp_and_predictive_modeling/) and while we're at it, let's also load the labels and the same ids we want to use for training and testing models in all our NLP blogs. 
# MAGIC 
# MAGIC - **reviews.csv**: a csv file with review texts - the fuel for our NLP analyses. (included key: restoreviewid, hence the unique identifier for a review) 
# MAGIC - **labels.csv**: a csv file with 1 / 0 values, indicating whether the review is a review for a Michelin restaurant or not (included key: restoreviewid)
# MAGIC - **trainids.csv**: a csv file with 1 / 0 values, indicating whether the review should be used for training or testing - we already split the reviews in train/test to enable reuse of the same samples for fair comparisons between techniques (included key: restoreviewid)

# COMMAND ----------

# Read data files from public blob storage
    
# **reviews.csv**: a csv file with review texts - the fuel for our NLP analyses. (included key: restoreviewid, hence the unique identifier for a review) 
reviews <- read.csv(file = 'https://bhciaaablob.blob.core.windows.net/cmotionsnlpblogs/reviews.csv',header=TRUE,stringsAsFactors=FALSE)

# **labels.csv**: a csv file with 1 / 0 values, indicating whether the review is a review for a Michelin restaurant or not (included key: restoreviewid)
labels <- read.csv(file = 'https://bhciaaablob.blob.core.windows.net/cmotionsnlpblogs/labels.csv',header=TRUE,stringsAsFactors=FALSE)

# **trainids.csv**: a csv file with 1 / 0 values, indicating whether the review should be used for training or testing - we already split the reviews in train/test to enable reuse of the same samples for fair comparisons between techniques (included key: restoreviewid)storage_download(cont, "blogfiles/labels.csv",overwrite =TRUE)
trainids <- read.csv(file = 'https://bhciaaablob.blob.core.windows.net/cmotionsnlpblogs/trainids.csv',header=TRUE,stringsAsFactors=FALSE)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Tokenize reviews 
# MAGIC 
# MAGIC Now that we have our textual data, we can have a look at the tokenizer at work. Let's tokenize an example from our review text to see what happens. In previous NLP blogs we use the reviewTextClean column of our dataset. This text was completely cleaned from interpunction, stopwords, abbreviations etcetera. Here we follow a different apprach as word order matters. So we are keeping as many words as possible and remove interpunction we do not need. 

# COMMAND ----------

# Remove metatext ('b:'), replace all non alpha characters and extra spaces except the end of sentence '.' character. 
reviews <- reviews %>% mutate(reviewText=gsub("[^[:alnum:][:space:].]", "",substr(reviewText,3,nchar(reviewText)-1)))

cat('# original text of sample review:\n')
print(reviews$reviewText[5])

cat('\n# tokenized sample review:\n')
tokenizer$encode(reviews$reviewText[5])

# COMMAND ----------

# MAGIC %md
# MAGIC We can see that the tokenizer results in a list with the text translated in the token indices, starting with the id for the `CLS` token (1) and finishing with the id for the `SEP` token (2). Notice that we only see one `SEP` token here and that points at the end of sentences (`.`) are represented by token id 13, which is treated the same as any other normal token. During pretraining, texts needed to be split in different sentences using this `SEP` token to predict sentence order. For finetuning, we don't have to train this task, therefore it is not needed to specify segments within the text sequences. If you would like to fully retrain BERT including the next sentence prediction task, you would need to provide an input for the tokenizer where sentences are seperate list items. In that case, in the output you can see the `SEP` token halfway: 

# COMMAND ----------

# Example with two sentences 
print(tokenizer$encode('Zeer goed eten op een ruime locatie. Vrij parkeren voor de deur was ideaal.', 'Zijn bekend met de Indiase keuken sinds 1985. En we zijn vertrokken zonder te betalen.'))

# COMMAND ----------

# MAGIC %md
# MAGIC What's good to know is that improvements on BERT (specifically Roberta) have shown that the next sentence prediction (NSP) task is actually not needed. Therefore, later BERT variants focus in pretraining on the Masked Language Model (MLM) task.  
# MAGIC 
# MAGIC What's also good to know is that BERT doesn't only look up words in the token dictionary but also splits complex (in fact unknown) words into subwords: BERT uses a WordPiece tokenization strategy. If a word is out-of-vocabulary (OOV), then BERT will break it down into word pieces it does know. For instance the word 'aspergesoep' (EN: Aspargus soup) might not be in the 30K vocabulary, but its parts 'asperge' and '##soep' are. And as a result, sequences are likely to become somewhat longer after tokenizing than the original sequence, also because BERT adds some extra functional tokens like the begining of a sentence [CLS], a seperator between sentences [SEP] or a padding token [PAD]. Good to keep this in mind when we have to specify the max length of input texts for BERT!

# COMMAND ----------

complex_token <- 'aspergesoep'

cat('# tokenize a complex word:',complex_token,' \n')
tokenized_example <- tokenizer$encode(complex_token)
print(tokenized_example)

cat('\n# lookup the token ids in the dictionary:\n')
print(tokenizer$convert_ids_to_tokens(tokenized_example))

cat('\n# decode word back to original:\n')
print(tokenizer$decode(tokenized_example))

# COMMAND ----------

# MAGIC %md Below is an example of how a sentence is encode into id's and how is can be decoded back to its original form. 

# COMMAND ----------

example <- tokenizer$encode('ik wil dit vest graag ruilen, het is te klein', max_length=250, truncation=T, add_special_tokens=T, padding=T)
print(tokenizer$convert_ids_to_tokens(example))
print(tokenizer$decode(example))

# COMMAND ----------

# MAGIC %md
# MAGIC Before we can finetune BERTje with our restaurant review texts in the task to predict which of those are reviews for Michelin restaurants and which are reviews for non-Michelin restaurants, we need to tokenize our textual data. We split our files into train and test datasets with the same mapping as we did earlier (identical IDs). 

# COMMAND ----------

# split reviews and labels into train and test
train_texts <- trainids %>% left_join(y=reviews, by= "restoReviewId", match = "all") %>% filter(train == 1) %>% select(reviewText) %>% pull()
test_texts <- trainids %>% left_join(y=reviews, by= "restoReviewId", match = "all") %>% filter(train == 0) %>% select(reviewText) %>% pull()

train_labels <- trainids %>% left_join(y=labels, by= "restoReviewId", match = "all") %>% filter(train == 1) %>% select(ind_michelin) %>% pull() %>% as.array()
test_labels <- trainids %>% left_join(y=labels, by= "restoReviewId", match = "all") %>% filter(train == 0) %>% select(ind_michelin) %>% pull() %>% as.array()

# COMMAND ----------

# MAGIC %md Next we apply the BERTje tokenizer to the reviewText field of all reviews. We need to specify the maximum review length here, since the tokenizer will cut off too long reviews and add padding to make all reviews the same length. In our earlier NLP blogs, we used a maximum review length of 150 tokens. Remember that BERT adds some special tokens to the sequence and splits complex words into multiple tokens. Also we did not exclude stopwords from the corpus, so the length of reviews increased. We increase the max_length for BERT therefore to 250 tokens. 

# COMMAND ----------

# Encode text so it contains a dictionary with input ids, token ids and mask ids
train_encodings = tokenizer(train_texts, truncation=TRUE, padding=TRUE,max_length=250L)
test_encodings = tokenizer(test_texts, truncation=TRUE, padding=TRUE,max_length=250L)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Use BERT model without finetuning
# MAGIC 
# MAGIC In a minute, we will finetune BERTje for our classification task. But first, let's show why finetuning is a good idea. You might wonder: If BERT is so great in translating full texts of different lengths into embeddings that can be used for different downstream tasks, why not just get those embeddings and use them for predictions? Not a bad idea, this saves you on expensive finetuning. Yes, finetuning is much faster and cheaper than training a BERT model from scratch, but still takes some GPU's and computation hours. So, before we do finetune BERTje for our predictions, we just run our texts through the pretrained model and extract the embeddings for the full text - hence the embeddings for the CLS token:

# COMMAND ----------

# Add last hidden layer for full text (CLS token) to feature matrix
BERTje = transformer$TFBertModel$from_pretrained("wietsedv/bert-base-dutch-cased")
ntexts_train = length(train_texts)
ntexts_test = length(test_texts)
features_train = matrix(NA, nrow=ntexts_train, ncol=768) 
features_test = matrix(NA, nrow=ntexts_test, ncol=768) 

# Extraction for the train dataset (!our implementation to extract the embeddings is quite slow! This might take a couple of hours!)
for (i in 1:(ntexts_train)){
  encodings_i = tokenizer(train_texts[i], truncation=TRUE, padding=TRUE,max_length=250L, return_tensors='tf')
  features_train[i,] = py_to_r(array_reshape(BERTje(encodings_i)[[1]][[0]][[0]],c(1, 768)))
 }

# And the same recipe for the test dataset
for (i in 1:(ntexts_test)){
  encodings_i = tokenizer(test_texts[i], truncation=TRUE, padding=TRUE,max_length=250L, return_tensors='tf')
  features_test[i,] = py_to_r(array_reshape(BERTje(encodings_i)[[1]][[0]][[0]],c(1, 768)))
 }


# COMMAND ----------

# MAGIC %md Now, we have transformed all our different-length review texts into same-length (768) numeric vectors we can use in any model we want. Next we setup a simple `Keras` sequential model taking these CLS embeddings as our only input. The input share is the batch size x 768 columns from the last BERT layer. We apply some additional hidden layers for training. 

# COMMAND ----------

# Build our model with the CLS output
model_CLS <- keras_model_sequential() %>% 
  # Specify the input shape
  layer_dense(units = 100, activation = "relu", input_shape = ncol(features_train)) %>% 
  # add a dense layer with 40 units
  layer_dense(units = 40, activation = "relu", kernel_initializer = "he_normal", bias_initializer = "zeros", kernel_regularizer = regularizer_l2(0.05)) %>% layer_dropout(rate = 0.2) %>%
  # add the classifier on top
  layer_dense(units = 1, activation = "sigmoid") 

summary(model_CLS)

# COMMAND ----------

model_CLS %>% compile(
  optimizer = "Adam",
  # we have a binary classification, a single unit sigmoid in the dense layer so binary_crossentropy 
  loss = "binary_crossentropy",
  # plot accuracy against train and testset
  metrics = c("acc", "AUC")
)

history <- model_CLS %>% fit(
  features_train, train_labels,
  # maximum number of iterations, since we did not add any new information we will keep it at 1
  epochs = 20, 
  # how many reviews do we offer in each batch
  batch_size = 512,
  # we have little Michelin restaurants, so we need to focus more on classifying these (set weights)
  class_weight = list("0"=1,"1"=32),
  # check train results againts test data
  validation_data = list(features_test, test_labels)
  #validation_split = 0.2
)

# COMMAND ----------

# Use model to predict probability of Michelin star on test data (you can also predict_classes if wanted)
CLS_result <- as.data.frame(predict(model_CLS, features_test))

# Add the real label to the dataframe
CLS_result$actual  <- test_labels

# From Keras we get a probability > convert to label, cut-off at 0.5
CLS_result <- CLS_result %>% mutate(predict = case_when(V1 >= .5 ~1, TRUE ~0)) %>% rename(probability = V1)

# Display results in a table
confusion_matrix <- table(CLS_result$actual, CLS_result$predict, dnn = c('actual', 'predicted'))      
print(confusion_matrix)

TP <- confusion_matrix[2,2] # True Positives
FP <- confusion_matrix[1,2] # False Positives
FN <- confusion_matrix[2,1] # False Negatives
TN <- confusion_matrix[1,1] # True Negatives

#Accuracy: What % of all predictions are correct?
Accuracy = (TP+TN)/(TP+FP+FN+TN)
cat('\n Accuracy:   ', scales::percent(Accuracy),' of all Michelin/non-Michelin review predictions are correct')

#Precision: What % of predicted Michelin reviews are actually Michelin reviews? 
Precision = (TP)/(TP+FP)
cat('\n Precision:  ', scales::percent(Precision),' of predicted Michelin reviews are actually Michelin reviews')

#Recall/Sensitivity: What % of all actual Michelin reviews are predicted as such? 
Recall = (TP)/(TP+FN)
cat('\n Recall:     ', scales::percent(Recall),' of all actual Michelin reviews are predicted as such')

#F1.Score = weighted average of Precision and Recall
F1.Score = 2*(Recall * Precision) / (Recall + Precision)
cat('\n F1 score:  ', round(F1.Score,2),' is the weighted average of Precision and Recall')

# COMMAND ----------

# MAGIC %md The model reaches an AUC of .89, certainly not better than the word embedding models we used in our [previous article](https://www.theanalyticslab.nl/nlpblogs_4_using_word_embedding_models_for_prediction_purposes/). To put things in perspective the plot below shows the performance of the best word embedding model, the random forest model using topic modeling and the BERTje model without additional training. In our earlier NLP posts we have introduced modelplotr, a package that can display insightful plots for multiple models at once. These plots are all based on the predicted probability distribution instead of the 'hard' prediction based on a cutoff value. Let's explore how well we can predict Michelin reviews with the models built with BERTje compared to the best Word Embedding model and to the Random Forest model using Topic Modeling. The code below will generate the input needed for modelplotr and plot results. The graph is very clear: the BERTje CLS model is better than the topic modeling model but has a lot to gain to throw the word embedding model from the stage.   

# COMMAND ----------

options(repr.plot.width=600, repr.plot.height=600)

# COMMAND ----------

# For scoring of Keras models input as a list is required
input_test = list(features_test)

# Score models based on text only and save validation predictions in dataframe
scores_and_ntiles_BERTje_CLS <- prepare_scores_and_ntiles_keras(inputlist=list('input_test'),
                                                                  inputlist_labels=list('test data'),
                                                                  outputlists=list('test_labels'),
                                                                  models = list('model_CLS'),
                                                                  model_labels=list('BERTje CLS (NN)'),
                                                                  ntiles = 100)

# Create plot input
scores_and_ntiles_BERTje_CLS <- plotting_scope(prepared_input = scores_and_ntiles_BERTje_CLS, scope="compare_models", select_dataset='test data')

# Putting input together for visualisation
plot_input <- rbind(scores_and_ntiles_BERTje_CLS, scores_and_ntiles_RF, scores_and_ntiles_GLoVe)

# Plot model performance
plot_input <- rbind(scores_and_ntiles_BERTje_CLS, scores_and_ntiles_RF, scores_and_ntiles_GLoVe)
plot_cumgains(plot_input, custom_line_colors=c("#FFDC51", "#000000", "#003D7C"))  

# COMMAND ----------

# MAGIC %md The general CLS model used here simply uses the outcome of the 12th layer, flattened to a sentence representation embedding (using pooling) to perform the classification. But you might also [argue](https://arxiv.org/pdf/1905.05583.pdf) that each attention head captures different characteristics. So instead of using the ouput from the final layer other choices can be made, like [summing the last four hidden layers](http://jalammar.github.io/illustrated-bert/). We will not iterate upon these other options in this article, but do know that these options exist. A nice example can be [found here](https://towardsdatascience.com/nlp-extract-contextualized-word-embeddings-from-bert-keras-tf-67ef29f60a7b).

# COMMAND ----------

# MAGIC %md
# MAGIC #### Specify BERT model for finetuning
# MAGIC 
# MAGIC We move on in our endavour by finetuning a BERT model for our classification task. We already prepared our training data in the previous steps so it's time to configure our pretrained BERTje model for *finetuning*. Let us take a closer look at a simplification of how the BERT model is trained. This image was taken from the introduction blog on BERT by [Chris McCormick](https://mccormickml.com/2019/07/22/BERT-fine-tuning/). The BERT model has 12 Transformer layers. The vector representations of all tokens are encoded and decoded in each layer. The output token embeddings are summarized in the 12th layer. Another great graphical breakdown of BERT can be found [here](http://jalammar.github.io/illustrated-bert/).  
# MAGIC 
# MAGIC 
# MAGIC !['BERT learning'](https://bhciaaablob.blob.core.windows.net/cmotionsnlpblogs/CLS_token_500x606.png)<br><br>
# MAGIC 
# MAGIC From the Hugging Face 🤗 library you can download various pre-trained model setups. You can download a general model and add additional layers for your downstream tasks, a masking model if you want to perform next word or sentence prediction. Since we have a specific, supervised task - using the full text as input (predict michelin reviews using the review texts) - we need a classification model. Hugging Face provides a 'ForSequenceClassification' model setup ready for such a prediction task, all we need to do is to download the model and specify the training arguments. We follow [this approach](https://huggingface.co/transformers/custom_datasets.html) from the Hugging Face documentation and customize to R and our own data.

# COMMAND ----------

training_args = transformer$TFTrainingArguments(
    output_dir='./results',           # output directory
    num_train_epochs=1,               # total number of training epochs
    per_device_train_batch_size=16L,  # batch size per device during training
    per_device_eval_batch_size=128L,  # batch size for evaluation
    warmup_steps=100L,                # number of warmup steps for learning rate scheduler
    weight_decay=0.01,                # strength of weight decay
    logging_dir='./logs',             # directory for storing logs
    logging_steps=10L
)

# Download model, the 'TFAutoModel' part of the model specification will make sure you get the right TensorFlow model version from the repository. 
model_BERTje = with(training_args$strategy$scope(),transformer$TFAutoModelForSequenceClassification$from_pretrained("wietsedv/bert-base-dutch-cased"))

# COMMAND ----------

# MAGIC %md When you download a model from Hugging Face 🤗 you can always look at the confuguration of the model when it was build and trained. The BERTje model is a classic BERT model with 12 hidden layers, 768 units per layer and 12 attention heads. The vocabulary size is 30.000 and it has an impressive number of parameters we can finetune: 

# COMMAND ----------

# How was this model trained?
model_BERTje$config

# COMMAND ----------

# Number of parameters in the BERTje model
model_BERTje$count_params()

# COMMAND ----------

# MAGIC %md
# MAGIC A few steps back we tokenized our texts. When using Hugging Face's Transformer module to finetune the Tensorflow BERTje model, the input has to be a tuple containing a dictionary with the token ids, the token type ids, the attention mask as well as the labels. Below we prepare the Tensorflow input.  

# COMMAND ----------

# Python tensorflow package and python builtin methods needed to prepare tensorflow input 
train_dataset = tf$data$Dataset$from_tensor_slices(tuple(builtins$dict(train_encodings),train_labels))
test_dataset = tf$data$Dataset$from_tensor_slices(tuple(builtins$dict(test_encodings),test_labels))

# Result is a python dictionary ready to be used as input for the model
str(train_dataset)

# COMMAND ----------

# MAGIC %md
# MAGIC To finetune the BERTje model for our prediction task, we can create a trainer and train our BERTje model. Happily, pretraining is already done on an enormous corpus of Dutch text. The finetuning will alter parameters slightly to better match our task: distinguishing Michelin reviews from non-Michelin reviews. Since the model still has an impressive 109 million parameters to tweak, it still takes an hour or two on our 2-GPU cluster for just a single epoch.  

# COMMAND ----------

trainer = transformer$TFTrainer(
    model=model_BERTje,                  # the instantiated 🤗 Transformers model to be trained
    args=training_args,                  # training arguments, defined above
    train_dataset=train_dataset          # training dataset
)

# Train the model
trainer$train()

# COMMAND ----------

# MAGIC %md
# MAGIC Let's have a look at how good this finetuned model is in predicting Michelin Reviews. Like we did in the previous blogs, we reviewed some metrics (AUC and confusion matrix related stats) and a few plots. First we need to get predictions for our test data, which is unseen during training and the same test data we used in our previous blogs. We use the trainer we just finetuned for our task to predict on the test data. 

# COMMAND ----------

# Explore the predictions object
predictions <- trainer$predict(test_dataset)
str(predictions)

# COMMAND ----------

# MAGIC %md
# MAGIC The predictions object is a list with the predicted logits per output class (in our case: Michelin review/non-Michelin review) per test review and the actual label per reviews (1=Michelin Review, 0=no-Michelin Review). The transformer module does not provide us with class probabilities but returns raw model outputs. For instance, the output for one review looks like this: `[3.121,-1.102]`. Therefore, we first apply a softmax transformation to each prediction to get class probabilities between 0% and 100% summing to 100% for each review, turning predictions into something like `[0.976,0.024]`). 

# COMMAND ----------

# transform logit scores into probabilities and prepare dataframe for evaluations
probabilities <- t(apply(predictions['predictions'], 1, function(x) exp(x) / sum(exp(x))))
actual <- predictions['label_ids']   
predicted <- max.col(probabilities)-1
                         
# confusion matrix: actual vs predicted counts
confmat   <- table(actual,predicted)
                         
# derive True Positive, False Positive, False Negative and True Negative from confusion matrix
TP <- confmat[2,2]; FP <- confmat[1,2]; FN <- confmat[2,1]; TN <- confmat[1,1]

#Accuracy: What % of all predictions are correct?
Accuracy = (TP+TN)/(TP+FP+FN+TN)
cat('\n Accuracy:   ', scales::percent(Accuracy),' of all Michelin/non-Michelin review predictions are correct')

#Precision: What % of predicted Michelin reviews are actually Michelin reviews? 
Precision = (TP)/(TP+FP)
cat('\n Precision:  ', scales::percent(Precision),' of predicted Michelin reviews are actually Michelin reviews')

#Recall (also known as Sensitivity): What % of all actual Michelin reviews are predicted as such? 
Recall = (TP)/(TP+FN)
cat('\n Recall:     ', scales::percent(Recall),' of all actual Michelin reviews are predicted as such')

#F1.Score = weighted average of Precision and Recall
F1.Score = 2*(Recall * Precision) / (Recall + Precision)
cat('\n F1 score:  ', round(F1.Score,2),' is the weighted average of Precision and Recall')

# COMMAND ----------

# MAGIC %md 
# MAGIC Let's put things in perspective and compare prediction results from the models we have up until now. The statistics from the confusion matrix are the best we've seen so far! Let's also compare the models visually using `modelplotr`. The code below will generate the input needed for modelplotr, combine scores from previous models and plot results.  

# COMMAND ----------

# Labeling for modelplotr                 
probs <- data.frame(cbind(probabilities,actual))
names(probs) <- c('prob_0','prob_1',"y_true")
probs$y_true = as.factor(probs$y_true)
ntiles = 100
cutoffs = c(stats::quantile(probs$prob_1,probs = seq(0,1,1/ntiles),na.rm = TRUE))
probs$ntl_1 <- (ntiles+1)-as.numeric(cut(probs$prob_1,breaks=cutoffs,include.lowest=TRUE))
probs$ntl_0 <- (ntiles+1)-probs$ntl_1
probs$model_label=factor('BERTje (TF)')
probs$dataset_label=factor('test data')
scores_and_ntiles_BERTje <- aggregate_over_ntiles(probs[,c("model_label","dataset_label","y_true","prob_0","prob_1","ntl_0","ntl_1")])              

# Create plot input
scores_and_ntiles_BERTje <- plotting_scope(prepared_input = scores_and_ntiles_BERTje, scope="compare_models", select_dataset='test data')

# Putting input together for visualisation
plot_input <- rbind(scores_and_ntiles_BERTje_CLS, scores_and_ntiles_BERTje, scores_and_ntiles_RF, scores_and_ntiles_GLoVe)

# Plot model performance
plot_cumgains(plot_input, custom_line_colors=c("#FFDC51", "#F5A507", "#000000",  "#003D7C"))  


# COMMAND ----------

# MAGIC %md 
# MAGIC That is a quite impressive cumulative gains chart! The BERTje finetuned model outperforms our best word embedding model. After 5% of all cases the finetuned BERTje model retrieves nearly 81% of all reviews related to a Michelin star restaurant. Our best performing word embedding model detected 68% of those reviews at 5% of alle cases. Maybe we can do **even better**, let's look at a competing transformer model to see if we further improve performance.

# COMMAND ----------

# MAGIC %md ## Use a competing transformer model: RobBERT
# MAGIC 
# MAGIC Let's look at a competing transformer model build for the Dutch language. Scholars from the Paul G. Allen School of Computer Science & Engineering,University of Washington and Facebook AI found that the original BERT model was undertrained. To overcome this the researchers trained a new model called [RoBERTa](https://arxiv.org/pdf/1907.11692.pdf). This new model was trained much longer, used bigger data batches, has no next sentence prediction objective, is trained on longer sequences and has a dynamic masking pattern. The Dutch variant of RoBERTa is called [RobBERT](https://github.com/iPieter/RobBERT) and was [developed](https://arxiv.org/pdf/2001.06286.pdf) by the University of Leuven and Berlin. The model was trained on a Dutch corpus of 39GB with 6.6 billion words over 126 million lines of text. As a reference, the BERTje model was trained on *only* a 12GB corpus. 
# MAGIC 
# MAGIC Below we download the model van the transformer package and set up the RobBERT tokenizer to get our text ready for input. 

# COMMAND ----------

# Get the RobBERT tokenizer in place
tokenizer <- transformer$AutoTokenizer$from_pretrained('pdelobelle/robbert-v2-dutch-base')

# COMMAND ----------

# Encode text so it contains a dictionary with input ids, token ids and mask ids
train_encodings = tokenizer(train_texts, truncation=TRUE, padding=TRUE,max_length=250L)
test_encodings = tokenizer(test_texts, truncation=TRUE, padding=TRUE,max_length=250L)

# Python tensorflow package and python builtin methods needed to prepare tensorflow input 
train_dataset = tf$data$Dataset$from_tensor_slices(tuple(builtins$dict(train_encodings),train_labels))
test_dataset = tf$data$Dataset$from_tensor_slices(tuple(builtins$dict(test_encodings),test_labels))

# COMMAND ----------

# MAGIC %md We use the same training arguments as for the BERTje model, fit the model, extract performance metrics and plot results to compare.  

# COMMAND ----------

training_args = transformer$TFTrainingArguments(
    output_dir='./results',           # output directory
    num_train_epochs=1,               # total number of training epochs
    per_device_train_batch_size=16L,  # batch size per device during training
    per_device_eval_batch_size=128L,  # batch size for evaluation
    warmup_steps=100L,                # number of warmup steps for learning rate scheduler
    weight_decay=0.01,                # strength of weight decay
    logging_dir='./logs',             # directory for storing logs
    logging_steps=10L
)

# COMMAND ----------

# Load RobBERT model from the transformer library
model_RobBERT = with(training_args$strategy$scope(),transformer$TFAutoModelForSequenceClassification$from_pretrained("pdelobelle/robbert-v2-dutch-base"))

# Create trainer
trainer = transformer$TFTrainer(
    model=model_RobBERT,                # the instantiated 🤗 Transformers model to be trained
    args=training_args,                 # training arguments, defined above
    train_dataset=train_dataset         # training dataset
)

# Train the model
trainer$train()

# COMMAND ----------

# predict finetuned RobBERT model
predictions <- trainer$predict(test_dataset)

# transform logit scores into probabilities and prepare dataframe for evaluations
probabilities <- t(apply(predictions['predictions'], 1, function(x) exp(x) / sum(exp(x))))
actual <- predictions['label_ids']   
predicted <- max.col(probabilities)-1
                         
# confusion matrix: actual vs predicted counts
confmat   <- table(actual,predicted)
print(confmat)
                         
# derive True Positive, False Positive, False Negative and True Negative from confusion matrix
TP <- confmat[2,2]; FP <- confmat[1,2]; FN <- confmat[2,1]; TN <- confmat[1,1]

#Accuracy: What % of all predictions are correct?
Accuracy = (TP+TN)/(TP+FP+FN+TN)
cat('\n Accuracy:   ', scales::percent(Accuracy),' of all Michelin/non-Michelin review predictions are correct')

#Precision: What % of predicted Michelin reviews are actually Michelin reviews? 
Precision = (TP)/(TP+FP)
cat('\n Precision:  ', scales::percent(Precision),' of predicted Michelin reviews are actually Michelin reviews')

#Recall (also known as Sensitivity): What % of all actual Michelin reviews are predicted as such? 
Recall = (TP)/(TP+FN)
cat('\n Recall:     ', scales::percent(Recall),' of all actual Michelin reviews are predicted as such')

#F1.Score = weighted average of Precision and Recall
F1.Score = 2*(Recall * Precision) / (Recall + Precision)
cat('\n F1 score:  ', round(F1.Score,2),' is the weighted average of Precision and Recall')

# COMMAND ----------

# MAGIC %md Baseds upon the values in the confusion matrix results for the RobBERT model look very much like the values we saw when estimating the finetuned BERTje model. In the graph below, using `modelplotr` we can even see that the BERTje model outperforms the RobBERT model by a few percent. At the fifth ntile the RobBERT model has 76% of Michelin star restaurant reviews detected against 81% for the BERTje model. 

# COMMAND ----------

# preparing for modelplotr                 
probs <- data.frame(cbind(probabilities,actual))
names(probs) <- c('prob_0','prob_1',"y_true")
probs$y_true = as.factor(probs$y_true)
ntiles = 100
cutoffs = c(stats::quantile(probs$prob_1,probs = seq(0,1,1/ntiles),na.rm = TRUE))
probs$ntl_1 <- (ntiles+1)-as.numeric(cut(probs$prob_1,breaks=cutoffs,include.lowest=TRUE))
probs$ntl_0 <- (ntiles+1)-probs$ntl_1
probs$model_label=factor('RobBERT (TF)')
probs$dataset_label=factor('test data')
scores_and_ntiles_RobBERT <- aggregate_over_ntiles(probs[,c("model_label","dataset_label","y_true","prob_0","prob_1","ntl_0","ntl_1")])              

# Create plot input RobBERT
scores_and_ntiles_RobBERT <- plotting_scope(prepared_input = scores_and_ntiles_RobBERT, scope="compare_models", select_dataset='test data')

# Putting input together for visualisation
plot_input <- rbind(scores_and_ntiles_RobBERT, scores_and_ntiles_BERTje, scores_and_ntiles_RF, scores_and_ntiles_GLoVe)

# Plot model performance
plot_cumgains(plot_input, custom_line_colors=c("#ACACAC", "#F5A507", "#000000",  "#003D7C"))  


# COMMAND ----------

# MAGIC %md 
# MAGIC There's not much difference in the predictions between BERTje and RobBERT in terms of the confusion matrix statistics and the modelplotr plots. Both finetuned models do an excellent job in predicting Michelin reviews. 
# MAGIC 
# MAGIC ### Another option: multilingual distilBERT
# MAGIC We noted before that there are also some multilingual variants of BERT. These multilingual versions are not specifically trained on one language like BERTje and RobBERT are but are trained on a corpus of documents for 104 different languages. One of those is distilBERT, a distilled version of BERT that is [smaller, faster, cheaper and lighter](https://medium.com/huggingface/distilbert-8cf3380435b5) than BERT without losing significant performance. We don't show the code here since this is very much the same as for BERTje and RobBERT but do show the performance below. We used the transformer model `distilbert-base-multilingual-cased`. 

# COMMAND ----------

# Preparing plot inclusing DistilBERT results
plot_input <- rbind(scores_and_ntiles_DistilBERT, scores_and_ntiles_RobBERT, scores_and_ntiles_BERTje, scores_and_ntiles_RF, scores_and_ntiles_GLoVe)

# Add a custom title
my_plot_text <- customize_plot_text(plot_input = plot_input)
my_plot_text$cumgains$plottitle <- 'Comparison of all NLP models'

# Show results of all 3 transformer models
plot_cumgains(plot_input, custom_plot_text = my_plot_text, custom_line_colors=c("#7C3F00", "#ACACAC", "#F5A507", "#000000", "#003D7C" ))

# COMMAND ----------

# MAGIC %md You can see it in the graph and the results below speak for themselves, the Dutch BERTje model is the **overall best performing NLP model for our downstream task**: extract knowledge from restaurant review texts to predict which of those reviews are written for Michelin restaurants and which are reviews for non-Michelin restaurants. 
# MAGIC 
# MAGIC <img src ='https://raw.githubusercontent.com/wietsedv/bertje/master/bertje.png' width="300" height="300">

# COMMAND ----------

# MAGIC %md ### Wrapping it up
# MAGIC 
# MAGIC In this article we used transformer models to predict which restaurant is more likely to receive a next Michelin star. We showed that, when finetuned, these transformer models do an excellent job to retrieve important information from the review texts. There's not much difference between a number of variants but all of them outperform our predictions using custom topic models and word embeddings. Getting these models up and running does come at a price. Using a Transformer model, as we did in this article, required quite some (expensive) GPU time, whereas fitting the word embedding models in `Keras` was perfectly doable on a midsized laptop. So think twice before you start: how accurate do my predictions need to be? And is the trade-off between hardware investment and a better accuracy positive in a production environment?   
# MAGIC 
# MAGIC Another note is that you might question how well the original BERT model training tasks are suited for the classification task we are performing. From [research](https://arxiv.org/pdf/1901.09785.pdf) it is known that a model trained for a specific task performs better than a general model with a large amount of generic data. One might also question whether the data we have gathered (restaurant review) is generic enough ;) How many times is discussing the menu, the taste of a dish or the servant present in Wikipedia text? An option would be not just to fintune an existing transformer model but to build your own transformer model using BERT architecture, the same way we did for our word embeddings. That would require to have a substansive amount of reviews available for training.
# MAGIC 
# MAGIC Here we've shown you how to use transformer models for text classification using a bit of `Python` (importing models from Hugging Face) and building a model in `R` using `Keras`. Hopefully you are as enthusiastic as we are about NLP, and we hope that these blogs help you to extract valuable information from text to the task at hand. 
# MAGIC 

